﻿#if UNITY_EDITOR
#define DESKTOP
#elif (UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_WP8_1)
#define MOBILE
#else
#define DESKTOP
#endif


using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(AudioSource))]

public class VideoPlayer : Singleton<VideoPlayer>
{
	public AudioSource audioSource;
	private AudioClip audio;
	// Event for when video is played
	public delegate void VideoEndedEventHandler(VideoPlayer m, EventArgs e);
	public event VideoEndedEventHandler videoEndedEvent;

#if DESKTOP
	private MovieTexture video;
	private IEnumerator hideWhenVideoEndsCoroutine;

	public void playVideo(string videoUrl)
	{
		StartCoroutine(loadAndPlayMovieTexture(videoUrl));
	}

	private IEnumerator loadAndPlayMovieTexture(string videoPath)
	{

		string path = "file:///" + Application.streamingAssetsPath + "/" + videoPath + ".ogg";
		WWW videoWWW = new WWW(path);

		// Loading file before continuing
		while (videoWWW.isDone == false)
		{
			yield return 0;
		}

		// Saving the movieTexture
		this.video = videoWWW.GetMovieTexture();
		this.audio = this.video.audioClip;
		this.audioSource.clip = this.audio;
		// Setting the movie to the videoplayer plane texture
		gameObject.GetComponent<Renderer>().material.mainTexture = this.video;

		this.video.Play();
		this.audioSource.Play();

		bringToFront();

		// Starting coroutine that check's when video ends
		hideWhenVideoEndsCoroutine = hideWhenVideoEnds();
		StartCoroutine(hideWhenVideoEndsCoroutine);
	}

	// TODO Get event that gamemanager gives and listens to. Launch that event when movie ends
	private IEnumerator hideWhenVideoEnds()
	{
		while (true)
		{
			if (video.isPlaying == false)
			{
				Debug.Log("Video stopped");
				// Hiding player
				sendToBack();
				// Stopping playback
				this.audioSource.Stop();
				this.video.Stop();

				// Launching event, that video ended
				if (videoEndedEvent != null)
				{
					videoEndedEvent(this, null);
				}

				break;
			}
			// Checks every frame if video ended
			yield return 0;
		}
	}

	public void bringToFront()
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, -1);
	}
	public void sendToBack()
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, 1);
	}

#endif
#if MOBILE
	public void playVideo(string videoUrl)
	{
		// Setting video and audio
		//this.audio = audio;
		//this.audioSource.clip = this.audio;

		// Playing video on mobile
		StartCoroutine(playMobileVideo(videoUrl));
	}

	private IEnumerator playMobileVideo(string videoUrl)
	{
		// Adding .mp4 to the url, which is the mobile video version
		videoUrl += ".mp4";	

		Debug.Log("Streaming asset: " + Application.streamingAssetsPath);
		// Outputted: Streaming asset: jar:file:///data/app/com.UnasNet.MobileGame-2/base.apk!/assets
		
		Debug.Log("Playing: " + videoUrl);
		Handheld.PlayFullScreenMovie(videoUrl, Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFill);
		
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		Debug.Log("Video playback completed.");
		audioSource.Stop();

		// Launching event, that video ended
		if (videoEndedEvent != null)
		{
			videoEndedEvent(this, null);
		}
	}
#endif
}
