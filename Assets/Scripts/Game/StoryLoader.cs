﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.IO;

public class StoryLoader : MonoBehaviour {

	private string XMLStr;

	private XmlSerializer readSerializer;
	private FileStream fs;

	// Use this for initialization
	void Start () {
		this.XMLStr = LockScreenManager.instance.selectedStory.XML;

		readSerializer = new XmlSerializer(typeof(UN.Config.MobileGameConfig));
		// Reading the XML string
		Stream stream = GenerateStreamFromString(XMLStr);
		GameManager.instance.gameConfig = (UN.Config.MobileGameConfig)readSerializer.Deserialize(stream);
		stream.Close();

		// Saving game info to gamemanager
		GameManager.instance.gameName = LockScreenManager.instance.selectedStory.storyName;
		GameManager.instance.gameFolder = LockScreenManager.instance.selectedStory.storyFolder;

		// Now that the story is loaded, telling gamemanager to start the phone, not story
		GameManager.instance.Start();

		// TODO Make a better solution for the loading
		// Giving Gamemanager some time to load

		StartCoroutine(gameManagerLoadWait());
	}

	private IEnumerator gameManagerLoadWait()
	{
		yield return new WaitForSeconds(2.0f);

		GameManager.instance.startStory();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Stream GenerateStreamFromString(string s)
	{
		MemoryStream stream = new MemoryStream();
		StreamWriter writer = new StreamWriter(stream);
		writer.Write(s);
		writer.Flush();
		stream.Position = 0;
		return stream;
	}
}
