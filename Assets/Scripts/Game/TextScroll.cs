﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class TextScroll : Singleton<TextScroll> {

	private IEnumerator scrollTextAnimationCoroutine;
	private Canvas canvas;
	private Text text_;
	private string text
	{
		get
		{
			return text_.text;
		}
		set
		{
			text_.text = value;
		}
	}

	// Use this for initialization
	void Awake () {

		text_ = gameObject.GetComponentInChildren<Text>();
		canvas = gameObject.GetComponent<Canvas>();
	}

	/// <summary>
	/// Brings the text to the view of the player
	/// </summary>
	public void BringToFront()
	{
		canvas.sortingOrder = 300;
	}

	/// <summary>
	/// Hides the text from the player
	/// </summary>
	public void SendToBack()
	{
		canvas.sortingOrder = -1;
	}

	public delegate void TextEndedHandler(TextScroll m, EventArgs e);
	public event TextEndedHandler textEndedEvent;
	/// <summary>
	/// Show's the given text with the speed given. Component is hidden after hideAfter (seconds). Event textEndedEvent is launched after completion.
	/// </summary>
	/// <param name="letterPerSecond"></param>
	/// <param name="hideAfter"></param>
	/// <param name="hideCanvasAfter"></param>
	/// <param name="text"></param>
	public void scrollText(float letterPerSecond, float hideTextAfter, float hideCanvasAfter, string text, bool hideCanvas)
	{
		// Emptying text
		this.text = "";

		// Showing canvas
		BringToFront();

		// Starting animation
		scrollTextAnimationCoroutine = scrollTextAnimation(letterPerSecond, hideTextAfter, hideCanvasAfter, text, hideCanvas);
		StartCoroutine(scrollTextAnimationCoroutine);
	}
	private IEnumerator scrollTextAnimation(float letterPerSecond, float hideTextAfter, float hideCanvasAfter, string text, bool hideCanvas)
	{
		float timePassed = 0f;
		while (true)
		{
			// Updating how much time has passed
			timePassed += Time.deltaTime;
			// Showing text, speed determines how many we are to show every update
			int lettersToShow = (int)(timePassed * letterPerSecond);

			if (lettersToShow >= text.Length){
				// If text is shown completely
				this.text = text;

				break;
			}
			this.text = text.Substring(0, lettersToShow);

			yield return 0;
		}

		// Waiting for the time we are suppose to
		yield return new WaitForSeconds(hideTextAfter);

		// Removing text
		this.text = "";

		// Waiting for the time until canvas is suppose to be hid
		yield return new WaitForSeconds(hideCanvasAfter);
		if (hideCanvas)
		{
			SendToBack();
		}

		// Launching event that text ended
		if (textEndedEvent != null)
		{
			textEndedEvent(this, null);
		}
	}
}
