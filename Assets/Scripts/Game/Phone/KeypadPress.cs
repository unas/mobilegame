﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class KeypadPress : MonoBehaviour {

	public Sprite pressedKeypadImage;

	private Image button;
	private Sprite normalSprite;
	

	// Use this for initialization
	void Start () {
		button = GetComponent<Image>();
		normalSprite = button.sprite;
	}

	public void keyDown()
	{
		if (pressedKeypadImage != null && normalSprite != null)
		{
			button.sprite = pressedKeypadImage;
		}
	}

	public void keyUp()
	{
		if (pressedKeypadImage != null && normalSprite != null)
		{
			button.sprite = normalSprite;
		}
	}

}
