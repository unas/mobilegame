﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Number : MonoBehaviour {

	public int maximumNumbers;

	private Text number;

	// Use this for initialization
	void Start () {
		number = GetComponent<Text>();
		
		// Clearing number text
		number.text = "";
	}

	public void addNumber(string number)
	{
		// Checking that we wont go beyond the number length limit
		if (this.number.text.Length < maximumNumbers)
		{
			this.number.text += number;
		}
	}

	public void clearNumbers()
	{
		this.number.text = "";
	}
}
