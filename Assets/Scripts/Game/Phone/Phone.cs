﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UN.Config;
using System;

enum PhoneView
{
	KEYPAD,
	LOGS,
	CONTACTS,
	CALL
};

public class Phone : MonoBehaviour {

	public GameObject phoneNavigation;
	public GameObject keypadView;
	public GameObject logsView;
	public GameObject contactsView;
	
	// Audio
	private AudioSource audioSource;

	// Call
	public GameObject callView;
	public GameObject answerChoices;
	public UnityEngine.UI.Text callViewCallerText;
	public Image callViewCallerIcon;
	public Image callViewAnswerButton;
	public Image callViewDeclineButton;
	private IncomingCall incomingCall;
	private int? currentDialogIndex;
	private Contact caller;

	private PhoneView currentPhoneView = PhoneView.KEYPAD;

	private AudioClip ringtone;
	private AudioClip answerSound;

	#region Events

	public delegate void CallEndedHandler(Phone m, int? unlockNode, int? launchEvent);
	public event CallEndedHandler callEndedEvent;

	void Awake()
	{
		// Getting the audiosource
		this.audioSource = GetComponent<AudioSource>();
	}
	
	// Use this for initialization
	void Start () {

		// Hiding all views, except the current one
		hideViews(getCurrentView());

		// Loading answer sound
		//AudioClip ringtone = Resources.Load<AudioClip>("Stories/" + gameFolder + "/Audio/" + gameConfig.configuration.incomingCallSound);
		//answerSound = Resources.Load<AudioClip>("Sounds/answer");
		string ding = "Stories/" + GameManager.instance.gameFolder + "/Audio" + GameManager.instance.gameConfig.configuration.answerCallSound;
		answerSound = Resources.Load<AudioClip>("Stories/" + GameManager.instance.gameFolder + "/Audio/" + GameManager.instance.gameConfig.configuration.answerCallSound);
	}

	public void setRingtone(AudioClip ringtone)
	{
		this.ringtone = ringtone;
	}

	

	#endregion

	#region show/hide

	/// <summary>
	/// Hide's all view, except the one given in parameter, that one will be shown
	/// </summary>
	/// <param name="skipView"></param>
	private void hideViews(GameObject showView = null)
	{
		this.keypadView.SetActive(this.keypadView == showView);
		this.logsView.SetActive(this.logsView == showView);
		this.contactsView.SetActive(this.contactsView == showView);
		this.callView.SetActive(this.callView == showView);
		this.answerChoices.SetActive(this.answerChoices == showView);
	}

	/// <summary>
	/// Return's the object of the current view
	/// </summary>
	/// <returns></returns>
	private GameObject getCurrentView()
	{
		if (currentPhoneView == PhoneView.KEYPAD)
		{
			return keypadView;
		}
		else if (currentPhoneView == PhoneView.LOGS)
		{
			return logsView;
		}
		else if (currentPhoneView == PhoneView.CONTACTS)
		{
			return contactsView;
		}

		// Returning by default the keypad
		return keypadView;
	}

	public void showKeypad()
	{
		hideViews(keypadView);
	}
	public void showLogs()
	{
		hideViews(logsView);
	}
	public void showContacts()
	{
		hideViews(contactsView);
	}

	#endregion

	#region call
	/// <summary>
	/// Brings the call view up, because someone is calling you
	/// </summary>
	/// <param name="caller"></param>
	/// <param name="incomingCall"></param>
	public void startIncomingCall(Contact caller, IncomingCall incomingCall)
	{
		this.caller = caller;
		this.incomingCall = incomingCall;

		hideViews(callView);
		// Showing answer button, incase it was hidden after call was answered
		this.callViewAnswerButton.enabled = true;
		
		// Setting caller picture and number/name
		if (caller != null)
		{
			// If the caller number is null, that means the number is unknown
			if (caller.number == null)
			{
				callViewCallerText.text = "Unknown caller";
			}
			else if (caller.nickname != "" || caller.firstname != "" || caller.lastname != "")
			{
				if (caller.nickname != "")
				{
					callViewCallerText.text = (caller.firstname + " \"" + caller.nickname + "\" " + caller.lastname).Trim();
				}
				else
				{
					callViewCallerText.text = (caller.firstname + " " + caller.lastname).Trim();
				}
			}
			else
			{
				callViewCallerText.text = caller.number;
			}

			// TODO Set image
			//callerImage.sprite = (Sprite)Resources.Load(caller.icon);
			
		}

		// Starting ringtone
		//GameManager.instance.audioSource.clip = ringtone;
		//GameManager.instance.audioSource.Play();
		this.audioSource.clip = ringtone;
		this.audioSource.Play();
	}

	// When pressing the answer button on the call view, this method will be called
	// Connection made in unity to call this event when answer button is pressed
	public void answerIncomingCall()
	{
		// Stopping ringtone
		this.audioSource.Stop();
		// Hiding the answer button
		callViewAnswerButton.enabled = false;
		// Playing the answer sound
		this.audioSource.clip = answerSound;
		this.audioSource.Play();

		StartCoroutine(startIncomingCallConversation());
	}

	private IEnumerator startIncomingCallConversation()
	{
		if (this.incomingCall != null)
		{
			// Waiting until audiosource stops playing (answer sound etc.)
			yield return new WaitWhile(() => this.audioSource.isPlaying);

			// Starting audio dialog
			// Getting first dialog number
			if (this.incomingCall.dialogs.Count > 0)
			{
				currentDialogIndex = this.incomingCall.dialogs.OrderBy(x => x.order).First().order;
				Dialog currentDialog = this.incomingCall.dialogs.Single(x => x.order == currentDialogIndex);

				StartCoroutine(continueIncomingCallConversation(currentDialog));
			}
			else
			{
				throw new Exception("No dialogs in incoming call!");
			}
		}
	}
	private IEnumerator continueIncomingCallConversation(Dialog dialog)
	{
		String soundFile = "Stories/" + GameManager.instance.gameFolder + "/" + dialog.soundFile;

		AudioClip dialogAudio = Resources.Load<AudioClip>(soundFile);
		// Playing dialog
		this.audioSource.clip = dialogAudio;
		this.audioSource.Play();

		// Waiting until sound ends, then showing dialog options
		yield return new WaitWhile(() => this.audioSource.isPlaying);

		// If there are dialog options
		if (dialog != null && dialog.answerOptions != null && dialog.answerOptions.Count > 0)
		{
			this.answerChoices.SetActive(true);
			AnswerChoice answerChoiceScript = this.answerChoices.GetComponent("AnswerChoice") as AnswerChoice;
			answerChoiceScript.setAnswerOptions(dialog);

			// Listening for event when user selected an answeroption
			answerChoiceScript.answerChoiceEvent += new AnswerChoice.AnswerChoiceEventHandler(userSelectedCallAnswer);
		}
		// If there are no more dialogs, then the call will end
		else
		{
			this.endCall(null, dialog.callEndEvent);
		}
		
	}
	private void userSelectedCallAnswer(AnswerOption option)
	{
		// Getting next dialog
		Dialog nextDialog = this.incomingCall.dialogs.Single(x => x.order == option.nextDialog);

		// Closing answer dialog
		this.answerChoices.SetActive(false);

		StartCoroutine(continueIncomingCallConversation(nextDialog));
	}

	/// <summary>
	/// Brings the call view up, because player is calling someone
	/// </summary>
	/// <param name="personToCall"></param>
	/// <param name="call"></param>
	public void startOutgoingCall(Contact personToCall, Call call)
	{
		
	}
	/// <summary>
	/// Ending call, player or the person we called ended the call
	/// </summary>
	public void endCall(int? unlockNode, int? launchEvent)
	{
		// Launching event, that the call ended, gamemanager is listening to this
		if (callEndedEvent != null)
		{
			callEndedEvent(this, unlockNode, incomingCall.endCall.launchEvent);
		}
	}
	#endregion
}
