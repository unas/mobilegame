﻿#if UNITY_EDITOR
#define DESKTOP
#elif (UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_WP8_1)
#define MOBILE
#else
#define DESKTOP
#endif

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using System;
using UN.Config;

public enum GameStatus
{
	Prologue,
	Node,
	Epilogue
}


public class GameManager : Singleton<GameManager>
{
	private AudioSource audioSource_;
	public AudioSource audioSource
	{
		get
		{
			return audioSource_;
		}
	}

	#region GameConfig
	public string gameName = "unknown";
	public string gameFolder = "unknown";
	public UN.Config.MobileGameConfig gameConfig;
	public GameStatus gameStatus;
	public int currentNodeOrder = -1;
	public Node currentNode = null;
	public int currentMEventOrder = -1;
	public MEvent currentMEvent = null;
	#endregion

	#region Canvases
	public Canvas background;
	public Canvas notificationBar;
	public Canvas home;
	public Canvas homeButton;
	public Canvas phone;
	private Phone phoneScript;
	public Canvas applications;
	//public Canvas contacts;
	public Canvas messages;
	#endregion

	#region Notifications
	public GameObject newContactPrefab;
	#endregion

	public override void Awake()
	{
		// Calling base awake from Singleton
		base.Awake();

		// Getting audiosource
		audioSource_ = GetComponent<AudioSource>();
	}

	public void Start()
	{
		// Hiding all canvases, except home, background and notification bar
		Debug.Log("Start");
		background.enabled = true;
		notificationBar.enabled = true;
		home.enabled = true;
		homeButton.enabled = false;

		phone.enabled = false;
		// Getting phone script, so that we can call phone's public methods
		phoneScript = (Phone)this.phone.GetComponent(typeof(Phone));

		applications.enabled = false;
		//contacts.enabled = false;
		messages.enabled = false;

		// Starting video
		//VideoPlayer.instance.playVideo(Resources.Load<MovieTexture>("Stories/Whisper/Video/whisper_portlet_prologue"), Resources.Load<AudioClip>("Stories/Whisper/Audio/level1"));

		// Getting the ringtone and saving it
		AudioClip ringtone = Resources.Load<AudioClip>("Stories/" + gameFolder + "/Audio/" + gameConfig.configuration.incomingCallSound);
		phoneScript.setRingtone(ringtone);
	}

	#region show/hide views
	private void hideAllViews(Canvas doNotHide = null)
	{
		// If we are showing any other view than the home, then showing the home button
		homeButton.enabled = (doNotHide != home);

		// Hiding all views. If doNotHide is not null, then that canvas value will not be touched
		home.enabled = doNotHide != home ? false : home.enabled;
		phone.enabled = doNotHide != phone ? false : phone.enabled;
		applications.enabled = doNotHide != applications ? false : applications.enabled;
		//contacts.enabled = doNotHide != contacts ? false : contacts.enabled;
		messages.enabled = doNotHide != messages ? false : messages.enabled;
	}

	private void showView(Canvas objectToShow)
	{
		objectToShow.enabled = true;
		hideAllViews(objectToShow);
	}
	private void hideView(Canvas objectToHide)
	{
		objectToHide.enabled = false;
	}

	[Obsolete]
	public void showPhone()
	{
		phone.enabled = true;
		hideAllViews(phone);
	}
	[Obsolete]
	public void showHome()
	{
		home.enabled = true;
		hideAllViews(home);
	}
	[Obsolete]
	public void showApplications()
	{
		applications.enabled = true;
		hideAllViews(applications);
	}
	[Obsolete]
	public void showContacts()
	{
		//contacts.enabled = true;
		//hideAllViews(contacts);
	}
	[Obsolete]
	public void showMessages()
	{
		messages.enabled = true;
		hideAllViews(messages);
	}
	[Obsolete]
	public void showNotificationBar()
	{
		notificationBar.enabled = true;
	}
	[Obsolete]
	public void hideNotificationBar()
	{
		notificationBar.enabled = false;
	}
	[Obsolete]
	public void showHomeButton()
	{
		this.homeButton.enabled = true;
	}
	[Obsolete]
	public void hideHomeButton()
	{
		this.homeButton.enabled = false;
	}
	#endregion

	#region Notification Bar
	[SerializeField]
	private float batteryLevel_ = 100.0f;
	/// <summary>
	/// Battery per cent. Min 0, max 100. Values beyond these will be automatically changed
	/// </summary>
	public float batteryLevel
	{
		get
		{
			return batteryLevel_;
		}
		set
		{
			if (value < 0)
			{
				batteryLevel_ = 0;
			}
			else if (value > 100)
			{
				batteryLevel_ = 100;
			}
			else
			{
				batteryLevel_ = value;
			}
		}
	}
	[SerializeField]
	private float batteryDrainSpeed_ = 1.0f;
	/// <summary>
	/// How many per cent, per second the battery is drained
	/// </summary>
	public float batteryDrainSpeed
	{
		get
		{
			return batteryDrainSpeed_;
		}
	}
	/// <summary>
	/// How many time seconds between update
	/// </summary>
	private float batteryUpdateTime_ = 1.0f;
	public float batteryUpdateTime
	{
		get
		{
			return batteryUpdateTime_;
		}
	}
	#endregion

	#region Signal Strength
	[SerializeField]
	private float signalUpdateTime_ = 1.0f;
	public float signalUpdateTime
	{
		get
		{
			return signalUpdateTime_;
		}
	}
	[SerializeField]
	private float signalStrength_ = 100.0f;
	/// <summary>
	/// The signal strength per cent. Min 0, max 100. Values beyond these will be automatically changed
	/// </summary>
	public float signalStrength
	{
		get
		{
			return signalStrength_;
		}
		set
		{
			if (value < 0)
			{
				signalStrength_ = 0;
			}
			else if (value > 100)
			{
				signalStrength_ = 100;
			}
			else
			{
				signalStrength_ = value;
			}
		}
	}
	#endregion

	#region Story teller
	/// <summary>
	/// Story loader will call this function when gameconfig has been loaded to gamemanager and game is ready to be loaded
	/// </summary>
	public void startStory()
	{
		// Checking if there is an prologue we need to start with
		if (gameConfig.story.prologue != null && gameConfig.story.prologue.skipPrologue == false)
		{
			// Setting gameStatus
			gameStatus = GameStatus.Prologue;

			// Showing texts and videos in the order the config tells us to
			List<UN.Config.ProloguePlayOrder> prologuePlayOrder = getPrologueOrder();
			
			// Playing prologue
			playPrologue(prologuePlayOrder);
			
		}
		else if (gameConfig.story.nodes != null)
		{
			if (gameConfig.story.prologue != null && gameConfig.story.prologue.skipPrologue == true)
			{
				Debug.Log("Skipping prologue");
			}
			this.gameStatus = GameStatus.Node;
			Debug.Log("Starting nodes");
			startNodesFromStart();
		}
		else
		{
			throw new System.Exception("Can't start story, there is no prologue or nodes!");
		}
	}

	#region Prologue
	private List<UN.Config.ProloguePlayOrder> prologuePlayOrder;
	private void playPrologue(List<UN.Config.ProloguePlayOrder> playOrder)
	{
		prologuePlayOrder = playOrder;
		playNextPrologue();

	}

	private void playNextPrologue()
	{
		if (prologuePlayOrder.Count > 0 && prologuePlayOrder[0].video != null)
		{
			AudioClip prologueAudio = Resources.Load<AudioClip>(prologuePlayOrder[0].video.sound.file);

			// Getting path of the videofile
			string videoToLoadPath = prologuePlayOrder[0].video.file;

			// Removing this video, so that it wont be played again
			prologuePlayOrder.Remove(prologuePlayOrder[0]);

			// Listening for event when to continue on prologue
			VideoPlayer.instance.videoEndedEvent += new VideoPlayer.VideoEndedEventHandler(prologueVideoEnded);

			VideoPlayer.instance.playVideo(videoToLoadPath);
		}
		else if (prologuePlayOrder.Count > 0 && prologuePlayOrder[0].text != null)
		{
			// Showing text
			TextScroll.instance.textEndedEvent += new TextScroll.TextEndedHandler(prologueTextEnded);

			// TextScroll canvas will be hidden after text, if another text is not to be shown directly after
			bool hideCanvasAfterText = true;
			if (prologuePlayOrder.Count > 1 && prologuePlayOrder[1].text != null)
			{
				hideCanvasAfterText = false;
			}

			float lettersPerSecond = prologuePlayOrder[0].text.speed;
			float hideTextAfter = prologuePlayOrder[0].text.time;
			float hideCanvasAfter = prologuePlayOrder[0].text.timeAfter;
			string textToScroll = prologuePlayOrder[0].text.value;

			// Removing this text, so that it wont be shown again
			prologuePlayOrder.Remove(prologuePlayOrder[0]);

			TextScroll.instance.scrollText(lettersPerSecond, hideTextAfter, hideCanvasAfter, textToScroll, hideCanvasAfterText);
		}
		else
		{
			// Prologue is over, starting nodes from the beginning
			startNodesFromStart();
		}
	}
	private void prologueVideoEnded(VideoPlayer m, EventArgs e)
	{
		// Removing event subscription, so that it wont launch this, eg. in the main story
		VideoPlayer.instance.videoEndedEvent -= prologueVideoEnded;
		// Continuing with the prologue
		playNextPrologue();
	}
	private void prologueTextEnded(TextScroll m, EventArgs e)
	{
		// Removing event subscription, so that it wont launch this, eg. in the main story
		TextScroll.instance.textEndedEvent -= prologueTextEnded;
		// Continuing with the prologue
		playNextPrologue();
	}



	#endregion

	#region Random Story methods

	private void startNodesFromStart()
	{
		// Getting lowest node order number and starting it
		this.currentNodeOrder = gameConfig.story.nodes.Min(x => x.order);
		this.startNode(currentNodeOrder);
	}

	/// <summary>
	/// Read's from the story the node we are supposed to start
	/// If overrideEventOrder is given, then the event number will be started with in the given node
	/// </summary>
	/// <param name="nodeOrder"></param>
	/// <param name="overrideEventOrder"></param>
	public void startNode(int nodeOrder, int? overrideEventOrder = null)
	{
		// Okay, let's see what the script has in store for us today
		this.currentNode = gameConfig.story.nodes.Single(x => x.order == nodeOrder);
		this.currentNodeOrder = nodeOrder;

		if (currentNode.nodeStartEvent != null)
		{
			// Starting event
			int? eventOrder = overrideEventOrder != null ? overrideEventOrder : currentNode.nodeStartEvent;
			this.continueStory(this, null, eventOrder);
		}
	}

	/// <summary>
	/// Play's the event given in parameters
	/// </summary>
	/// <param name="mEvent"></param>
	public void startEvent(MEvent mEvent)
	{
		// Checking if there is an incoming call
		if (mEvent.incomingCall != null && mEvent.incomingCall.dialogs != null && mEvent.incomingCall.dialogs.Count > 0)
		{
			// Getting contact of the person calling us
			Contact caller = gameConfig.contacts.SingleOrDefault(x => Contact.convertNumber(x.number) == Contact.convertNumber(mEvent.incomingCall.number));
			if (caller == null)
			{
				// Creating empty contact, since we don't know the caller
				caller = new Contact();
				caller.number = mEvent.incomingCall.number;
			}

			// Starting the call
			startIncomingCall(caller, mEvent.incomingCall);
		}
		else if (mEvent.receiveContact != null && mEvent.receiveContact.Count > 0)
		{
			foreach (Contact newContact in mEvent.receiveContact)
			{
				// Adding contacts to gameConfig
				this.gameConfig.contacts.Add(newContact);
				StartCoroutine(showNewContactNotification(newContact));
			}

			// Checking if this event unlocks a node or guides us to the next event
			if (mEvent.unlockNode != null && mEvent.unlockNode > 0 || mEvent.nextEvent != null && mEvent.nextEvent > 0)
			{
				this.continueStory(this, mEvent.unlockNode, mEvent.nextEvent);
			}

		}
	}

	/// <summary>
	/// This method is called by events. E.g. when phone call is done, it call's this method to continue the story after that
	/// </summary>
	private void continueStory(System.Object caller, int? unlockNode, int? launchEvent)
	{
		// If we are supposed to unlock a node
		if (unlockNode != null && unlockNode > 0)
		{
			this.startNode((int)unlockNode, launchEvent);
		}

		// If we are suppose to launch another event
		else if (launchEvent != null)
		{
			startEvent(currentNode.MEvents.Single(x => x.order == launchEvent));
		}
	}

	private List<UN.Config.ProloguePlayOrder>getPrologueOrder()
	{
		// There can be videos and text that are shown in a prologue
		List<UN.Config.Video> prologueVideos = new List<UN.Config.Video>(gameConfig.story.prologue.videos);
		List<UN.Config.Text> prologueTexts = new List<UN.Config.Text>(gameConfig.story.prologue.texts);

		// Showing texts and videos in the order the config tells us to
		List<UN.Config.ProloguePlayOrder> prologuePlayOrder = new List<UN.Config.ProloguePlayOrder>();
		int totalAmount = prologueVideos.Count + prologueTexts.Count;
		for (int i = 0; i < totalAmount; ++i)
		{
			UN.Config.ProloguePlayOrder temp = new UN.Config.ProloguePlayOrder();
			int videoMin = -1;
			if (prologueVideos.Count > 0)
			{
				videoMin = prologueVideos.Min(x => x.order);
			}
			int textMin = -1;
			if (prologueTexts.Count > 0)
			{
				textMin = prologueTexts.Min(x => x.order);
			}

			// Checking which was smaller and then putting that to the prologuePlayOrder list
			if (videoMin < textMin && videoMin >= 0 || videoMin >= 0 && textMin < 0)
			{
				UN.Config.Video video = prologueVideos.Single(x => x.order == videoMin);
				temp.video = video;

				// Removing from prologueVideos, so that we wont add it again
				prologueVideos.Remove(video);
			}
			else if (textMin < videoMin && textMin >= 0 || textMin >= 0 && videoMin < 0)
			{
				UN.Config.Text text = prologueTexts.Single(x => x.order == textMin);
				temp.text = text;

				// Removing from prologueTexts, so that we wont add it again
				prologueTexts.Remove(text);
			}
			else
			{
				throw new System.Exception("Can't determine prologue play order. There is propably videos and texts with same order number, or negative order numbers!");
			}
			prologuePlayOrder.Add(temp);
		}

		return prologuePlayOrder;
	}
#endregion

#endregion

	#region Call
	/// <summary>
	/// Brings the call view up, because someone is calling you
	/// </summary>
	/// <param name="caller"></param>
	/// <param name="incomingCall"></param>
	public void startIncomingCall(Contact caller, IncomingCall incomingCall)
	{
		showView(this.phone);
		if (this.phoneScript != null)
		{
			this.phoneScript.startIncomingCall(caller, incomingCall);
			// Setting event for when call ends
			this.phoneScript.callEndedEvent += new Phone.CallEndedHandler(callEndedEventCallback);
		}
		else
		{
			Debug.Log("Can not call phoneScript, is null");
		}
	}
	/// <summary>
	/// Brings the call view up, because player is calling someone
	/// </summary>
	/// <param name="personToCall"></param>
	/// <param name="call"></param>
	public void startOutgoingCall(Contact personToCall, Call call)
	{
		showView(this.phone);
		this.phoneScript.startOutgoingCall(personToCall, call);
	}
	/// <summary>
	/// Ending call, player ended the call
	/// </summary>
	public void endCall()
	{
		showView(this.home);
		this.phoneScript.endCall(null, null);
	}
	private void callEndedEventCallback(Phone phone, int? unlockNode, int? launchEvent)
	{
		showView(this.home);
		this.continueStory(phone, unlockNode, launchEvent);
	}

	private IEnumerator showNewContactNotification(Contact newContact)
	{
		// Showing popup notification of the new contact
		GameObject notificationObject = Instantiate(this.newContactPrefab, transform.position, Quaternion.identity);
		//notificationObject.transform.SetAsLastSibling();

		// Getting notification script
		NewContactNotification notificationScript = notificationObject.GetComponent("NewContactNotification") as NewContactNotification;

		// Setting contact
		notificationScript.setNewContact(newContact);

		yield return new WaitForSeconds((float)this.gameConfig.configuration.notification.time);

		// Deleting notification
		Destroy(notificationObject);
	}
	#endregion
}
