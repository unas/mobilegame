﻿using UnityEngine;
using System.Collections;

public class Signal : MonoBehaviour {

	private Animator animator;
	private IEnumerator signalStrengthUpdateCoroutine;

	// Use this for initialization
	void Awake () {
		animator = GetComponent<Animator>();
	}

	void Start()
	{
		// These must be called in start. If they are called in awake, the game manager hasn't loaded up yet and causes canvas objects to be lost.
		signalStrengthUpdateCoroutine = signalStrengthUpdate();
		StartCoroutine(signalStrengthUpdateCoroutine);
	}

	// Update is called once per frame
	IEnumerator signalStrengthUpdate() {

		while (true)
		{
			animator.SetFloat("SignalStrength", GameManager.instance.signalStrength);

			yield return new WaitForSeconds(GameManager.instance.signalUpdateTime);
		}
	
	}
}
