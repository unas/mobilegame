﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UN.Config
{
	[Serializable]
	public class MobileGameConfig
	{
		public Configuration configuration;
		public List<Contact> contacts { get; set; }
		public List<Message> messages { get; set; }
		public Story story { get; set; }
	}

	public enum Sex
	{
		male,
		female
	};

	public class Story
	{
		public Prologue prologue;
		public List<Node> nodes;
		public List<Epilogue> epilogues;
	}

	public class Epilogue
	{
		public int order;
		public List<SaveValue> checkSaveValues;
		public int? launchText_;
		public int? launchText
		{
			get
			{
				return launchText_;
			}
			set
			{
				if (value < 0 || value == null)
				{
					launchText_ = null;
				}
				else
				{
					launchText_ = value;
				}
			}
		}
		public List<Text> texts;
	}

	public class Node
	{
		public int order;
		private int? nodeStartEvent_;
		public int? nodeStartEvent
		{
			get
			{
				return nodeStartEvent_;
			}
			set
			{
				if (value < 0 || value == null)
				{
					nodeStartEvent_ = null;
				}
				else
				{
					nodeStartEvent_ = value;
				}
			}
		}
		public List<Call> peopleYouCanCall;
		public List<SMS> peopleYouCanSMS;
		public List<MEvent> MEvents;
		public AppsYouCanUse appsYouCanUse;
	}

	public class Camera
	{
		public bool? canOpen;
		public string pictureToTake;
		private int peakTime_;
		public int peakTime
		{
			get
			{
				return peakTime_;
			}
			set
			{
				if (value < 0)
				{
					peakTime_ = 1000;
				}
				else
				{
					peakTime_ = value;
				}
			}
		}
		private int? pictureTakeEvent_;
		public int? pictureTakeEvent
		{
			get
			{
				return pictureTakeEvent_;
			}
			set
			{
				if (value < 0)
				{
					pictureTakeEvent_ = null;
				}
				else
				{
					pictureTakeEvent_ = value;
				}
			}
		}
		private int? pictureOpenEvent_;
		public int? pictureOpenEvent
		{
			get
			{
				return pictureOpenEvent_;
			}
			set
			{
				if (value < 0)
				{
					pictureOpenEvent_ = null;
				}
				else
				{
					pictureOpenEvent_ = value;
				}
			}
		}
		public List<PictureSendEvent> pictureSendEvents;

	}
	public class PictureSendEvent
	{
		public string number;
		public int launchEvent;
	}

	public class Fagbook
	{
		public bool? canOpen;
	}
	public class Dropbox
	{
		public bool? canOpen;
	}
	public class Whatsapp
	{
		public bool? canOpen;
	}
	public class AppsYouCanUse
	{
		public Camera camera;
		public Fagbook fagbook;
		public Dropbox dropbox;
		public Whatsapp whatsapp;
	}

	public class MEvent
	{
		public int order;
		public List<SaveValue> saveValues;
		public IncomingCall incomingCall;
		public IncomingSMS incomingSMS;
		private int? unlockNode_;
		public int? unlockNode
		{
			get
			{
				return unlockNode_;
			}
			set
			{
				if (value < 0)
				{
					unlockNode_ = null;
				}
				else
				{
					unlockNode_ = value;
				}
			}
		}
		public List<Contact> receiveContact;
		private int? nextEvent_;
		public int? nextEvent
		{
			get
			{
				return nextEvent_;
			}
			set
			{
				if (value < 0)
				{
					nextEvent_ = null;
				}
				else
				{
					nextEvent_ = value;
				}
			}
		}
		private int? waitTime_;
		public int? waitTime
		{
			get
			{
				return waitTime_;
			}
			set
			{
				if (value < 0)
				{
					waitTime_ = null;
				}
				else
				{
					waitTime_ = value;
				}
			}
		}
	}

	public class IncomingCall
	{
		private string number_;
		public string number
		{
			get
			{
				return number_;
			}
			set
			{
				if (value.ToUpper() == "UNKNOWN")
				{
					number_ = null;
				}
				else
				{
					number_ = value;
				}
			}
		}
		public List<Dialog> dialogs;
		public EndCall endCall;
	}

	public class IncomingSMS
	{
		public int order;
		public string number;
		public List<SMSDialog> SMSDialogs;
	}
	/*
	public class SMSDialog
	{
		public int order;
		public int launchEvent;
		public string value;

	}
	*/
	public class EndCall
	{
		private int? launchEvent_;
		public int? launchEvent
		{
			get
			{
				return launchEvent_;
			}
			set
			{
				if (value < 0)
				{
					launchEvent_ = null;
				}
				else
				{
					launchEvent_ = value;
				}
			}
		}
	}

	public class SMS
	{
		public string number;
		public List<SMSDialog> SMSDialogs;
	}

	public class SMSDialog
	{
		public int order;
		private int? launchEvent_;
		public int? launchEvent
		{
			get
			{
				return launchEvent_;
			}
			set
			{
				if (value < 0)
				{
					launchEvent_ = null;
				}
				else
				{
					launchEvent_ = value;
				}
			}

		}
		private DateTime? timestamp_;
		public DateTime? timestamp
		{
			get
			{
				return timestamp_;
			}
			set
			{
				timestamp_ = value;
			}
		}

		/// <summary>
		/// Returns the message from this dialog
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return this.value;
		}

		public List<SaveValue> saveValues; // If we come to this SMSDialog, then these values will be saved
		public string soundFile; // For music or something? I don't know, fuck you
		public string value; // What the person we are texting with is saying
							 //public SMSAnswer SMSAnswer; // The text that will user will answer when we come to the SMSDialog
		public List<SMSAnswerOption> SMSAnswerOptions;
	}
	/*
	public class SMSAnswer
	{
		public string value;
		public double time; // How long until this answer will be shown
	}
	*/
	public class SMSAnswerOption
	{
		public int order;
		public int NextSMSDialog;
		public string value;
		public List<SaveValue> saveValues;
	}

	public class Call
	{
		public string number;
		public List<Dialog> dialogs;
		public EndCall endCall;
	}

	public class Dialog
	{
		public int order;
		private int? callTime_;
		public int? callTime // Is this the first time calling the person that this dialog belongs to
		{
			get
			{
				return callTime_;
			}
			set
			{
				// If the value is less than 0, then that equals null
				if (value < 0)
				{
					callTime_ = null;
				}
				else
				{
					callTime_ = value;
				}
			}
		}
		public List<SaveValue> saveValues;
		private int? launchEvent_;
		public int? launchEvent
		{
			get
			{
				return launchEvent_;
			}
			set
			{
				// If the value is less than 0, then that equals null
				if (value < 0)
				{
					launchEvent_ = null;
				}
				else
				{
					launchEvent_ = value;
				}
			}
		}
		public string soundFile;
		private bool callDeclined_;
		public bool callDeclined // Will the call be declined
		{
			get
			{
				return callDeclined_;
			}
			set
			{
				if (value == null)
				{
					callDeclined_ = false;
				}
				else
				{
					callDeclined_ = value;
				}
			}
		}
		public List<AnswerOption> answerOptions;
		private int? callEndEvent_;
		public int? callEndEvent
		{
			get
			{
				return callEndEvent_;
			}
			set
			{
				if (value < 0)
				{
					callEndEvent_ = null;
				}
				else
				{
					callEndEvent_ = value;
				}
			}
		}
	}

	public class AnswerOption
	{
		public int order; // The order the options will be presented in
		public int nextDialog; // To what dialog order to jump to if this one is selected
		public string value; // The text that can be answered
	}

	// Used when showing prologue in correct order, to easily create object with either video or text
	public class ProloguePlayOrder
	{
		public Video video = null;
		public Text text = null;
	}

	public class Prologue
	{
		public bool skipPrologue;
		public List<Video> videos;
		public List<Text> texts;
	}

	public class Video
	{
		public int order;
		public List<CheckSaveValue> checkSaveValues;
		public string file;
		public Sound sound;
	}

	public class Sound
	{
		public string file;
		public bool playThroughStage;
	}

	public class Text
	{
		/// <summary>
		/// The ID. Tells us in what order to show stuff
		/// </summary>
		public int order;
		List<CheckSaveValue> checkSaveValues;
		/// <summary>
		/// How often the letters will be updated. Milliseconds.
		/// </summary>
		public int speed;
		/// <summary>
		/// How long until the text will be shown after the text has been shown completely, milliseconds
		/// </summary>
		public int time;
		/// <summary>
		/// How long until the next text is shown after this one has disapeared, milliseconds
		/// </summary>
		public int timeAfter;
		/// <summary>
		/// Sound file that will be played
		/// </summary>
		public string sound;
		/// <summary>
		/// If set to true, the soundfile will be looped
		/// </summary>
		public string loopSound;
		/// <summary>
		/// The text that shall be shown
		/// </summary>
		public string value;
	}

	public class SaveValue
	{
		public string value;
	}

	public class CheckSaveValue
	{
		public string saveValue;
		public string combineValue;
	}

	public class Configuration
	{
		public string gamename;
		public Battery battery;
		public string incomingCallSound;
		public string answerCallSound;
		public Notification notification;
	}

	public class Notification
	{
		public double time;
	}

	public class Battery
	{
		public int level;
		public double drainAmount;
		public double drainUpdatePerSecond;
		public int txtDrain;
		public double callDrainAmount;
		public int normalMusicAtBatteryLevel;
		public string normalMusicFile;
		public int tenseMusicAtBatteryLevel;
		public string tenseMusicFile;
		public int panicMusicAtBatteryLevel;
		public string panicMusicFile;
		public string finalMusicFile;
	}

	public class Contact
	{
		private string number_;
		public string number
		{
			get
			{
				return number_;
			}
			set
			{
				number_ = convertNumber(value);
			}
		}

		/// <summary>
		/// Converts telephone numbers +3585053... to 05053... If countrycode missing, same number is returned.
		/// </summary>
		/// <param name="numberToConvert"></param>
		/// <returns></returns>
		public static string convertNumber(string numberToConvert)
		{
			// Making sure parameter isn't null
			if (numberToConvert == null)
			{
				return null;
			}

			// Converting +35850... to 050
			if (numberToConvert.Length >= 4 && numberToConvert[0] == '+')
			{
				return numberToConvert.Remove(0, 4).Insert(0, "0");
			}
			else
				return numberToConvert;
		}

		public string firstname;
		public string lastname;
		public string nickname;
		public string fullName
		{
			get
			{
				string returnValue = "";
				if (this.nickname != null && this.nickname.Trim() != "")
				{
					returnValue = (this.firstname != null ? firstname.Trim() : "") + " \"" + this.nickname.Trim() + "\" " + (this.lastname != null ? this.lastname.Trim() : "");
				}
				else
				{
					returnValue = (this.firstname != null ? firstname.Trim() : "") + (this.lastname != null ? this.lastname.Trim() : "");
				}

				return returnValue.Trim();
			}
		}
		public string icon;
		public string sex;
		public string email;
		public string address;
		public Birthday birthday;
		public Company company;
		public string description;

		/// <summary>
		/// An empty contact card
		/// </summary>
		public Contact()
		{
			number = "";
			firstname = "";
			lastname = "";
			icon = "";
			sex = "";
			email = "";
			address = "";
			birthday = new Birthday();
			company = new Company();
			description = "";
		}
	}

	public class Company
	{
		public string name;
		public string address;
		public string number;

		public Company()
		{
			name = "";
			address = "";
			number = "";
		}
	}

	public class Birthday
	{
		[System.ComponentModel.DefaultValueAttribute(null)]
		public string day { get; set; }
		public string month { get; set; }
		public string year { get; set; }

		public Birthday()
		{
			day = "";
			month = "";
			year = "";
		}
	}

	public class Message
	{
		public bool receivedMessage;
		private string number_;
		public string number
		{
			get
			{
				return number_;
			}
			set
			{
				number_ = Contact.convertNumber(value); // Convert +35850 to 050. If already 050 then no change
			}
		}
		public string text;
		public DateTime timestamp;

		public override string ToString()
		{
			return text;
		}

		public static Message CreateMessage(Contact person, string message, bool receivedMessage, DateTime? timestamp = null)
		{
			UN.Config.Message returnValue = new Message();
			returnValue.number = Contact.convertNumber(person.number);
			returnValue.receivedMessage = receivedMessage;
			returnValue.text = message;
			returnValue.timestamp = timestamp == null ? DateTime.Now : (DateTime)timestamp;

			return returnValue;
		}
	}
}
