﻿using UnityEngine;
using System.Collections;

public class Battery : MonoBehaviour {

	private Animator animator;
	private IEnumerator batteryDrainCoroutine;

	void Awake()
	{
		animator = GetComponent<Animator>();
	}

	void Start () {
		// Since we don't want to update the battery lever every frame, we will start a coroutine that updates the battery level as defined in GameManager.batteryUpdateTime
		batteryDrainCoroutine = drainBattery();
		StartCoroutine(batteryDrainCoroutine);
	}

	IEnumerator drainBattery()
	{
		while (true)
		{


			GameManager.instance.batteryLevel -= GameManager.instance.batteryDrainSpeed * GameManager.instance.batteryUpdateTime;
			// Setting the value to animator, so that the correct frame is shown for the battery level
			animator.SetFloat("BatteryLevel", GameManager.instance.batteryLevel);

			// Updating once per second
			yield return new WaitForSeconds(GameManager.instance.batteryUpdateTime);
		}
	}
}
