﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class SlideToUnlock : MonoBehaviour {

	public float lockReturnSpeed = 350f; // How many values per second the slider is moved when released

	private Slider sliderInstance;
	private IEnumerator returnLockCoroutine;

	// Use this for initialization
	void Start () {

		// Getting the slider that is this script is connected to
		sliderInstance = GetComponent<Slider>();
	}

	public void EndDrag()
	{
		// If slider was dragged fully to the right, then unlocking (starting the game)
		if (sliderInstance.value == 100)
		{
			LockScreenManager.instance.unlockPhone();

		}
		else // Slider was not unlocked, returning to beginning
		{
			// Starting animation that returns the lock
			returnLockCoroutine = returnLock();
			StartCoroutine(returnLockCoroutine);
		}
	}

	public void OnDrag()
	{
		// If a coroutine is running that is trying to return the slider, stopping it
		if (returnLockCoroutine != null)
		{
			StopCoroutine(returnLockCoroutine);
		}
	}

	// This creates the animation that returns the lock
	private IEnumerator returnLock()
	{
		while (true)
		{
			float amountToMove = lockReturnSpeed * Time.deltaTime;
			if (sliderInstance.value < amountToMove)
			{
				sliderInstance.value = 0;
			}
			else
			{
				sliderInstance.value -= amountToMove;
			}

			if (sliderInstance.value <= 0)
			{
				break;
			}

			// This forces the update to happen only once per frame update
			yield return 0;
		}
	}
}
