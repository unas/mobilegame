﻿using UnityEngine;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class StoryFile
{
	public string XML = null;
	public string storyName = null;
	public string storyFolder = null;
}

public class LockScreenManager : Singleton<LockScreenManager> {

	public AudioSource musicPlayer;
	public AudioSource ding;
	public float fadeOutSpeed = 0.5f; // How fast per second music is faded out
	public Dropdown storySelection;

	private IEnumerator fadeMusicOutAndLoadGameCoroutine;
	private List<StoryFile> storySelectionFiles;
	private StoryFile selectedStory_ = null;
	public StoryFile selectedStory
	{
		get
		{
			return selectedStory_;
		}
	}
	
	public override void Awake()
	{
		// Calling base awake from Singleton
		base.Awake();

		// Setting up game selection
		if (SceneManager.GetActiveScene().name == "MainMenu")
		{
			List<Object> files = (Resources.LoadAll("Stories", typeof(TextAsset))).ToList();
			storySelectionFiles = new List<StoryFile>();

			// Clearing dropdown selections
			storySelection.options.Clear();

			for (int i = 0; i < files.Count; ++i)
			{
				StoryFile story = new StoryFile();
				TextAsset XML = (TextAsset)files[i];
				story.XML = XML.text;

				XDocument doc = XDocument.Parse(XML.text);

				var gamenames = doc.Descendants("gamename");

				foreach (var gameName in gamenames)
				{
					story.storyName = gameName.Value;
					story.storyFolder = gameName.Value;
					storySelectionFiles.Add(story);
					// Adding name of stories to the droplist
					storySelection.options.Add(new Dropdown.OptionData(gameName.Value));

					break;
				}
			}

			// Refreshing dropdown box, otherwise the values wont be seen
			storySelection.RefreshShownValue();
			selectedStory_ = storySelectionFiles != null && storySelectionFiles.Count > 0 ? storySelectionFiles[0] : null;
		}
		// For testing. If the game is directly started, then the XML is missing, putting whisper as default
		else if (SceneManager.GetActiveScene().name == "Game" && selectedStory == null)
		{
			Debug.Log("XML is null, setting default game whisper");
			StoryFile story = new StoryFile();
			TextAsset whisperFile = Resources.Load("Stories/Whisper/game") as TextAsset;
			story.XML = whisperFile.text;

			XDocument doc = XDocument.Parse(whisperFile.text);
			var gameNames = doc.Descendants("gamename");
			foreach (var gameName in gameNames)
			{
				story.storyName = gameName.Value;

				break;
			}
			story.storyFolder = "Whisper";

			selectedStory_ = story;
		}
	}

	public void storySelectionChanged()
	{
		string selectedGameName = storySelection.captionText.text;
		selectedStory_ = storySelectionFiles.FirstOrDefault(x => x.storyName == selectedGameName);
	}

	// Use this for initialization
	void Start ()
	{
		// Staring to play music
		if (musicPlayer != null)
		{
			musicPlayer.Play();
		}

		
	}

	public void unlockPhone()
	{

		fadeMusicOutAndLoadGameCoroutine = FadeMusicOutAndLoadGame("Game");
		StartCoroutine(fadeMusicOutAndLoadGameCoroutine);
	}

	private IEnumerator FadeMusicOutAndLoadGame(string nextScene)
	{
		// Playing ding sound
		ding.Play();

		while (true)
		{
			float amountToFade = fadeOutSpeed * Time.deltaTime;
			if (musicPlayer.volume <= amountToFade)
			{
				musicPlayer.volume = 0.0f;
				break;
			}
			musicPlayer.volume -= amountToFade;

			yield return 0; // Forces next update to be after next frame
		}

		// Starting game
		SceneManager.LoadScene("Game");
	}
}
