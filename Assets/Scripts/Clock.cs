﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Clock : MonoBehaviour
{

    private Text clockInstance;
    private IEnumerator clockCoroutine;

    void Start()
    {
        clockInstance = GetComponent<Text>();

        // Starting clock
        clockCoroutine = updateClock();
        StartCoroutine(clockCoroutine);
    }

    private IEnumerator updateClock()
    {
        while (true)
        {
            // Setting current time to the text object
            clockInstance.text = DateTime.Now.ToString("HH:mm");

            // Getting seconds until next time we need to update
            int waitForSecondsInt = 60 - DateTime.Now.Second;
            float waitForSeconds = waitForSecondsInt;

            // Sleeping until the minute changes
            yield return new WaitForSeconds(waitForSeconds);
        }
    }
}